# UIWidget Research Log

This is basically a go through with this tutorial series:
https://www.youtube.com/watch?v=1ukSR1GRtMU&list=PL4cUxeGkcC9jLYyp2Aoh6hcWuxFDX6PBJ

But instead of using dart and flutter, I used Unity UI Widget (v1.5.4) and translate native flutter code into UIWidget code.

We use Main.cs in each lesson as entry point, but split them with namespace to prevent error.

You will need to download package at https://github.com/Unity-Technologies/com.unity.uiwidgets/tree/release_1.5.4_stable
and install package by local to start

# Thoughts during writing it

## Pros
- Can actually use flutter api document as reference when developing
- Full support of styling and spacing as flutter
- State-base rendering (like React)
- gameplay ui & editor ui both supported
## Mixed
- the whole system is base on custom renderer, it will be hard to add new effect by our self
    - but on the other hand, it gave us performance boost, and solve the biggest problem unity ugui gave us
- Support loading file by resource and asset bundle, but only in sync but not async*
- lost advantage of unity inspector (since you can't serialize item to inspector due to Graphic Editor)
    - but on the other hand, you can declare everything by code
    - maybe this limitation can be by pass by Zenject?
## Cons
- newest version (>2.0) need to install specific version (China local version) of Unity to run
- doesn't included StreamBuilder widget for Rx stream setup (witch is one major feature for flutter/dart eco system)
- doesn't support lot of shortcut and helper in original flutter eco system (for example suggestion for moving widget, or widget outline etc.)