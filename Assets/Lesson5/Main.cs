﻿using System.Collections.Generic;
using Unity.UIWidgets.animation;
using Unity.UIWidgets.engine;
using Unity.UIWidgets.foundation;
using Unity.UIWidgets.material;
using Unity.UIWidgets.painting;
using Unity.UIWidgets.ui;
using Unity.UIWidgets.widgets;
using UnityEngine;
using FontStyle = Unity.UIWidgets.ui.FontStyle;

namespace UIWidgetTutorial.Lesson5
{
    public class Main : UIWidgetsPanel
    {
        protected override Widget createWidget()
        {
            return new MaterialApp(
                home: new Scaffold(
                    appBar: new AppBar(
                        title: new Text("My first app"),
                        centerTitle: true
                    ),
                    body: new Center(
                        child: new Text("Hello UIWidget")
                    ),
                    floatingActionButton: new FloatingActionButton(
                        child: new Text("Click")
                    )
                )
            );
        }
    }
}