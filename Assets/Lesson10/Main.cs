﻿using System.Collections.Generic;
using Unity.UIWidgets.animation;
using Unity.UIWidgets.engine;
using Unity.UIWidgets.foundation;
using Unity.UIWidgets.material;
using Unity.UIWidgets.painting;
using Unity.UIWidgets.ui;
using Unity.UIWidgets.widgets;
using UnityEngine;
using FontStyle = Unity.UIWidgets.ui.FontStyle;
using Image = Unity.UIWidgets.widgets.Image;

namespace UIWidgetTutorial.Lesson10
{
    public class Main : UIWidgetsPanel
    {
        public const string VisitorFontName = "visitor";

        protected override void OnEnable()
        {
            // add font from resource, set in OnEnable for preview to work
            // maybe we need to find other way to handle this?
            FontManager.instance.addFont(font: Resources.Load<Font>(path: "Fonts/visitor2"), VisitorFontName);
            // load material icon as special key so icon widget can work properly
            FontManager.instance.addFont(Resources.Load<Font>(path: "Fonts/MaterialIcons"), "Material Icons");
            base.OnEnable();
        }

        protected override Widget createWidget()
        {
            return new MaterialApp(
                home: new Home()
            );
        }
    }

    class Home : StatelessWidget
    {
        public override Widget build(BuildContext context)
        {
            return new Scaffold(
                appBar: new AppBar(
                    title: new Text("My first app"),
                    centerTitle: true,
                    backgroundColor: Colors.red[600]
                ),
                body: new Container(
                    color: Colors.grey[400],
                    child: new Text("Hello"),
                    padding: EdgeInsets.all(20), // there is a padding widget if you only need padding view
                    margin: EdgeInsets.all(20)
                ),
                floatingActionButton: new FloatingActionButton(
                    child: new Text("Click"),
                    onPressed: () => { },
                    backgroundColor: Colors.red.shade600 //alt way to select color
                )
            );
        }
    }
}