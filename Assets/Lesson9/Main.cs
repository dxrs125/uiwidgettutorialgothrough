﻿using System.Collections.Generic;
using Unity.UIWidgets.animation;
using Unity.UIWidgets.engine;
using Unity.UIWidgets.foundation;
using Unity.UIWidgets.material;
using Unity.UIWidgets.painting;
using Unity.UIWidgets.ui;
using Unity.UIWidgets.widgets;
using UnityEngine;
using FontStyle = Unity.UIWidgets.ui.FontStyle;
using Image = Unity.UIWidgets.widgets.Image;

namespace UIWidgetTutorial.Lesson9
{
    public class Main : UIWidgetsPanel
    {
        public const string VisitorFontName = "visitor";

        protected override void OnEnable()
        {
            // add font from resource, set in OnEnable for preview to work
            // maybe we need to find other way to handle this?
            FontManager.instance.addFont(font: Resources.Load<Font>(path: "Fonts/visitor2"), VisitorFontName);
            // load material icon as special key so icon widget can work properly
            FontManager.instance.addFont(Resources.Load<Font>(path: "Fonts/MaterialIcons"), "Material Icons");
            base.OnEnable();
        }

        protected override Widget createWidget()
        {
            return new MaterialApp(
                home: new Home()
            );
        }
    }

    class Home : StatelessWidget
    {
        public override Widget build(BuildContext context)
        {
            return new Scaffold(
                appBar: new AppBar(
                    title: new Text("My first app"),
                    centerTitle: true,
                    backgroundColor: Colors.red[600]
                ),
                body: new Center(
                    // part 1: icon; you need to set up font in Main before using it
                    // child: new Icon(
                    //     Icons.airport_shuttle,
                    //     color: Colors.lightBlue,
                    //     size: 50
                    // )

                    // part 2: button
                    // use RaisedButton for button with shadow
                    // child: new FlatButton(
                    //     child: new Text("Click Me"),
                    //     color: Colors.lightBlue,
                    //     onPressed: () =>
                    //     {
                    //         Debug.Log("You click me!");
                    //     }
                    // )

                    // part 3: button with icon
                    // child: RaisedButton.icon(
                    //     icon: new Icon(Icons.mail),
                    //     label: new Text("mail me"),
                    //     color: Colors.amber,
                    //     onPressed: () => { }
                    // )

                    // part 4: icon button
                    child: new IconButton(
                        icon: new Icon(
                            Icons.alternate_email,
                            color: Colors.amber
                        ),
                        onPressed: () =>
                        {
                            Debug.Log("You click me!");
                        }
                    )
                ),
                floatingActionButton: new FloatingActionButton(
                    child: new Text("Click"),
                    onPressed: () => { },
                    backgroundColor: Colors.red.shade600 //alt way to select color
                )
            );
        }
    }
}