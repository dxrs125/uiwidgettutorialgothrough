﻿using System.Collections.Generic;
using Unity.UIWidgets.animation;
using Unity.UIWidgets.engine;
using Unity.UIWidgets.foundation;
using Unity.UIWidgets.material;
using Unity.UIWidgets.painting;
using Unity.UIWidgets.ui;
using Unity.UIWidgets.widgets;
using UnityEngine;
using FontStyle = Unity.UIWidgets.ui.FontStyle;
using Image = Unity.UIWidgets.widgets.Image;

namespace UIWidgetTutorial.Lesson8
{
    public class Main : UIWidgetsPanel
    {
        public const string VisitorFontName = "visitor";

        protected override void OnEnable()
        {
            // add font from resource, set in OnEnable for preview to work
            // maybe we need to find other way to handle this?
            FontManager.instance.addFont(font: Resources.Load<Font>(path: "Fonts/visitor2"), VisitorFontName);
            base.OnEnable();
        }

        protected override Widget createWidget()
        {
            return new MaterialApp(
                home: new Home()
            );
        }
    }

    class Home : StatelessWidget
    {
        public override Widget build(BuildContext context)
        {
            return new Scaffold(
                appBar: new AppBar(
                    title: new Text("My first app"),
                    centerTitle: true,
                    backgroundColor: Colors.red[600]
                ),
                body: new Center(
                    // works same as below, the one below is a short cut
                    // child: new Image(
                    //     image: new AssetImage("Images/grass")
                    // )

                    // Image.asset support asset bundle if you're using it
                    child: Image.asset("Images/grass")
                ),
                floatingActionButton: new FloatingActionButton(
                    child: new Text("Click"),
                    onPressed: () => { },
                    backgroundColor: Colors.red.shade600 //alt way to select color
                )
            );
        }
    }
}