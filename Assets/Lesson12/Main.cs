﻿using System.Collections.Generic;
using Unity.UIWidgets.animation;
using Unity.UIWidgets.engine;
using Unity.UIWidgets.foundation;
using Unity.UIWidgets.material;
using Unity.UIWidgets.painting;
using Unity.UIWidgets.ui;
using Unity.UIWidgets.widgets;
using UnityEngine;
using FontStyle = Unity.UIWidgets.ui.FontStyle;
using Image = Unity.UIWidgets.widgets.Image;
using MainAxisAlignment = Unity.UIWidgets.rendering.MainAxisAlignment;
using CrossAxisAlignment = Unity.UIWidgets.rendering.CrossAxisAlignment;

namespace UIWidgetTutorial.Lesson12
{
    public class Main : UIWidgetsPanel
    {
        public const string VisitorFontName = "visitor";

        protected override void OnEnable()
        {
            // add font from resource, set in OnEnable for preview to work
            // maybe we need to find other way to handle this?
            FontManager.instance.addFont(font: Resources.Load<Font>(path: "Fonts/visitor2"), VisitorFontName);
            // load material icon as special key so icon widget can work properly
            FontManager.instance.addFont(Resources.Load<Font>(path: "Fonts/MaterialIcons"), "Material Icons");
            base.OnEnable();
        }

        protected override Widget createWidget()
        {
            return new MaterialApp(
                home: new Home()
            );
        }
    }

    class Home : StatelessWidget
    {
        public override Widget build(BuildContext context)
        {
            return new Scaffold(
                appBar: new AppBar(
                    title: new Text("My first app"),
                    centerTitle: true,
                    backgroundColor: Colors.red[600]
                ),
                body: new Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: new List<Widget>(){
                        new Row(
                            children: new List<Widget>()
                            {
                                new Text("Hello,"),
                                new Text("world")
                            }
                        ),
                        new Container(
                            color: Colors.cyan,
                            child: new Text("one"),
                            padding: EdgeInsets.all(20)
                        ),
                        new Container(
                            color: Colors.amber,
                            child: new Text("two"),
                            padding: EdgeInsets.all(30)
                        ),
                        new Container(
                            color: Colors.pink,
                            child: new Text("three"),
                            padding: EdgeInsets.all(40)
                        )
                    }
                ),
                floatingActionButton: new FloatingActionButton(
                    child: new Text("Click"),
                    onPressed: () => { },
                    backgroundColor: Colors.red.shade600 //alt way to select color
                )
            );
        }
    }
}